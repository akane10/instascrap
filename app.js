const express = require('express');
const puppeteer = require(`puppeteer`);
require('dotenv').config();

// document.querySelectorAll('span.g47SY')[0].textContent (total posts: string)

const app = express();
const port = process.env.PORT || 3000;
const server = app.listen(port, () =>
  console.log(`port ${port} is ready listening for request`)
);

app.use(express.static('static'));
app.use('*', (req, res) => {
  res.sendFile('static/index.html', { root: __dirname });
});

const arrayRemove = (arr, value) => arr.filter((ele) => ele != value);

const io = require('socket.io')(server);

const getHeight = async (page) => {
  const height = await page.evaluate(`document.body.scrollHeight`);
  return height;
};

const errIo = (key, err) => {
  return io.to(key).emit('error', err);
};

const doneIo = (key, message) => {
  return io.to(key).emit('done', message);
};

async function getImg(browser, url, key, io) {
  try {
    const page = await browser.newPage();

    await page.setExtraHTTPHeaders({
      'Accept-Language': 'en-US',
    });

    await page.goto(url, {
      waitUntil: `networkidle0`,
    });

    await page.waitFor(100);

    const img = await page.evaluate(() => {
      return Array.from(
        document.querySelectorAll('.kPFhm > .KL4Bh > img.FFVAD')
      ).map((i) => i.src);
    });

    await page.close();

    img.forEach((x) => {
      // console.log('emit');
      // console.log(x);
      return io.to(key).emit('scrap', x);
    });
  } catch (e) {
    console.error(e.message || e);
    throw new Error(e);
  }
}

// https://stackoverflow.com/questions/13143945/dynamic-namespaces-socket-io
io.on('connection', (socket) => {
  const { key } = socket.handshake['query'];
  // let browser = '';
  // let page = '';

  let loop = true;

  if (!key) {
    socket.disconnect();
    return { err: 'no key' };
  }

  console.log(`${key} connected`);
  socket.join(key);

  socket.on('disconnect', () => {
    console.log(`${key} disconnected`);
    loop = false;
    return socket.leave(key);
  });

  socket.on('scrap', async (data) => {
    try {
      console.log(data);
      const { instaId } = data;

      const url = `https://instagram.com/${instaId}`;
      const browser = await puppeteer.launch({
        // headless: false,
        // ignoreDefaultArgs: ['--disable-extensions'],
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
      });

      const page = await browser.newPage();

      console.log(url);

      await page.setExtraHTTPHeaders({
        'Accept-Language': 'en-US',
      });

      await page.goto(url, {
        waitUntil: `networkidle0`,
        timeout: 0,
      });

      console.log('target page', page._target._targetInfo.url);
      if (page._target._targetInfo.url.includes('login')) {
        return errIo(key, { message: 'Cannot scrap, login required' });
      }

      if (await page.$(`.dialog-404`)) {
        console.log(`User (${instaId}) instagram not found`);
        return errIo(key, { message: '404 not found' });
      }

      let medias = [];

      const getTotalPosts = await page.evaluate(() => {
        return Array.from(
          document.querySelectorAll('span.g47SY'),
          (e) => e.textContent
        )[0];
      });

      const totalPosts = getTotalPosts ? getTotalPosts.replace(',', '') : 0;

      while (medias.length < Number(totalPosts) && loop) {
        try {
          console.log('scraping...', Number(totalPosts));
          console.log(medias.length);
          const height = await getHeight(page);
          await page.evaluate(`window.scrollTo(0, document.body.scrollHeight)`);
          await page.waitForFunction(`document.body.scrollHeight > ${height}`);

          await page.waitFor(100);

          const newImgs = await page.evaluate(() => {
            return Array.from(
              document.querySelectorAll('a'),
              (e) => e.href
            ).filter((i) => i.includes('https://www.instagram.com/p/'));
          });

          for await (const img of newImgs) {
            if (!loop) return console.log('stopped');

            const isThere = medias.find((src) => src === img);

            if (!isThere) {
              await getImg(browser, img, key, io);
              medias.push(img);
            }
          }
        } catch (e) {
          console.error(e.message || e);
          throw new Error(e);
        }
      }

      doneIo(key, 'done');
      await browser.close();
    } catch (e) {
      console.error(e.message || e);
      return errIo(key, { message: e.message });
    }
  });
});
