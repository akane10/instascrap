const btn = document.getElementById('scrap');
const output = document.getElementById('output');
const instaId = document.getElementById('instaId');
const report = document.getElementById('report');
const imgs = document.getElementsByTagName('img');
const selectCount = document.getElementById('selectCount');

const baseUrl = 'http://localhost:3000/';

let arr = [];
let selected = [];

function select(e) {
  e.style.opacity = '0.1';
  e.setAttribute('onClick', 'unselect(this)');
  selected.push(e.src);
  selected = selected.filter((a, b) => selected.indexOf(a) === b);
  selectCount.innerHTML = `${selected.length} picture selected`;
}

function unselect(e) {
  e.style.opacity = '1';
  e.setAttribute('onClick', 'select(this)');
  selected = selected.filter((i) => i !== e.src);
  selectCount.innerHTML = `${selected.length} picture selected`;
}

function selectAll() {
  for (img of imgs) {
    select(img);
  }
}

function donlot() {
  console.log('downloaded');
}

const socket_connect = (room) => {
  return io.connect(baseUrl, {
    query: 'key=' + room,
  });
};

const random_room = Math.random().toString(36).substr(2, 5);

const socket = socket_connect(random_room);

socket.on('connect_failed', () => {
  report.innerHTML = '';
  report.innerHTML += `<p class="has-text-danger">connect_failed, no idea why</p>`;
});

socket.on('error', (err) => {
  btn.classList.remove('is-loading');
  // report.innerHTML = '';
  report.innerHTML += `<p class="has-text-danger">${err.message}</p>`;
});

socket.on('done', (message) => {
  btn.classList.remove('is-loading');
  report.innerHTML += `<p class="has-text-primary">${message}</p>`;
});

socket.on('disconnect', () => {
  btn.classList.remove('is-loading');
  report.innerHTML = '';
  report.innerHTML += `<p class="has-text-danger">Opps disconnect</p>`;
});

btn.addEventListener('click', () => {
  arr = [];
  selected = [];

  report.innerHTML = '';
  output.innerHTML = '';
  // selectCount.innerHTML = '';

  btn.classList.add('is-loading');
  report.innerHTML += `<p class="has-text-warning" >Start scraping https://instagram.com/${
    instaId.value || 'instagram'
  }</p>`;

  socket.emit('scrap', {
    instaId: instaId.value || 'instagram',
  });
});

socket.on('scrap', (data) => {
  arr.push(data);
  btn.classList.add('is-loading');
  report.innerHTML = '';
  report.innerHTML += `<p class="has-text-warning">Scraped ${arr.length} posts</p>`;

  output.innerHTML += `
  <div class="column is-2 is-narrow">
    <a href="${data}" download target="_blank">
      <img
      src="${data}"
      alt="image"
      />
    </a>
  </div>
`;
});
